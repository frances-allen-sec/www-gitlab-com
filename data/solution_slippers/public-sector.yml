title: GitLab for the Public Sector
description: Social coding, continuous integration, and release automation have proven to accelerate development and software quality to meet mission objectives. Learn more!
twitter_image: /images/opengraph/commit-opengraph.png
canonical_path: /solutions/public-sector/
file_name: public-sector.yml
header:
  title: 'GitLab for Public Sector'
  subtitle:
    'One application. Several opportunities to accelerate your speed to mission.'
  tagline: 'Trusted by government. Loved by developers.'
  cta: 'Start your free trial'
  destinationUrl: /free-trial
  ctaTwo: 'Questions? Contact us'
  destinationUrlTwo: /sales/
  image: https://gitlab-com.gitlab.io/marketing/inbound-marketing/slippers-ui/static/media/decsecops-survey-2021-landing_header.e4830e1d.svg
cards_section:
  heading: Speed. Efficiency. Trust.
  subtitle: GitLab helps enterprises innovate quickly with all-in-one CI/CD, source code management, and security.
  cards:
    - heading: Enabling Asynchronous Collaboration
      body: A modern software factory encourages the collaboration and teamwork needed to address the challenges of building and delivering applications. Break down silos to coordinate seamlessly across development, operations, and security with a consistent experience across the development lifecycle.
      icon: /images/icons/slp-collaboration.svg
    - heading: Quality, Accountability and Compliance
      body: The backbone of the software factory is the continuous integration pipeline which automates development tasks to be completed for every code change. Remediate code quality issues and report changes in real-time with centralized reporting and a single view for both development and security.
      icon: /images/icons/slp-lessrisk.svg
    - heading: QIncrease Operational Efficiency
      body: Simplify the software development toolchain to reduce total cost of ownership with a system that provides an efficient and collaborative developer experience. GitLab helps enterprises innovate quickly with all-in-one CI/CD, source code management, and security.
      icon: /images/icons/slp-increase.svg
tracks:
  heading: How does GitLab help government organizations become more productive, efficient and effective?
  body: A complete DevOps platform enables speed to mission delivery.
  card:
    - heading: 'Continuous Integration and Delivery'
      body: >-
        
        Build high-quality applications at scale with GitLab CI/CD. Accelerate your agency’s digital transformation journey,
        break down department silos, and streamline efficiency.
        


        - Ensure every code change is reasonable

        - Scale testing with parallel builds and flexible pipelines

        - Save time with auto-scaling CI/CD job runners

      icon_full: /images/icons/slp-cicd.svg
      cta: Learn about CI/CD
      href: /stages-devops-lifecycle/continuous-integration/
    - heading: Security
      body: >-
        Delivering software innovation faster can streamline your mission accelerating results, allowing
        your organization to better serve its citizens and protect the nation.
        

        - Strike a balance between managing risk and business agility

        - Secure the software supply chain with signed commits, preventing unauthorized pushes

        - View, triage, trend, track, and resolve vulnerabilities

      icon_full: /images/icons/slp-lock.svg
      cta: Learn about Application Security
      href: /solutions/dev-sec-ops/
    - heading: 'Agile Project Management'
      body: >-
        Gain visibility across your organization to deliver on time and on budget. Enable cross-functional 
        ollaboration and keep stakeholders connected with kanban boards, epics, and roadmaps.
        

        - Link issues with code changes

        - Manage sprints & backlogs

        - Connect strategy to execution

      icon_full: /images/icons/slp-agile.svg
      cta: Learn about Agile Delivery
      href: /solutions/agile-delivery/
    - heading: 'Source Code Management'
      body: >-
        Deliver better software faster with our enterprise-ready version control and collaboration. 
        Coordinate work, track and review changes, and manage delivery all in one interface.
        

        - Streamline code reviews with live previews

        - Leverage custom project templates and automated workflows

        - Manage projects and teams of any size
        
      icon_full: /images/icons/slp-sourcecode.svg
      cta: Learn about version control with GitLab
      href: /stages-devops-lifecycle/source-code-management/
cta_banner:
  - title: Free GitLab trial
    subtitle: Unlimited features to all features for 30 days.
    button_url: /free-trial
    button_text: Start your free trial
    destinationUrlTwo: /demo/
    ctaTwo: Watch a demo
satellite_events:
  heading: Upcoming Events
  body: Join us for an upcoming Public Sector event
  cards:
  - heading: Program and Portfolio Management Workshop for the Public Sector
    icon: /icons/slp-calendar.svg
    date: August 18, 2021
    event_type: Virtual Workshop'
    cta: Register Now
    destinationUrl: https://page.gitlab.com/portfolio-wsaug-registration-page.html
    noImageSource: true
  - heading: Cybersecurity Virtual Workshop for the Public Sector
    icon: /icons/slp-webcast.svg
    date: September 8, 2021
    event_type: Virtual Workshop'
    cta: Event Details
    destinationUrl: https://page.gitlab.com/security-Public-Sector-workshop-registration-sep-2021.html
    noImageSource: true
  - heading: Advanced CI/CD Workshop for Public Sector
    icon: /icons/slp-calendar.svg
    date: November 9, 2021
    event_type: Virtual Workshop
    cta: Event Details
    destinationUrl: #
    noImageSource: true
block_list_1:
  heading: Available Contracts
  decoration: true
  items: 
    - icon: /icons/slp-contracts.svg
      text: NASA SWEP
    - icon: /icons/slp-contracts.svg
      text: LevelUP BOA
    - icon: /icons/slp-contracts.svg
      text: OTAs
    - icon: /icons/slp-contracts.svg
      text: ITED-SW2
    - icon: /icons/slp-contracts.svg
      text: FedResults GSA
markto:
  heading: Contact GitLab’s Public Sector Sales team
  body: >-
    ### GitLab Federal, LLC information


    -   **Federal Tax ID:** 61-1917524

    -   **DUNS Number:** 116955701

    -   **CAGE Code:** 89TZ1

    -   **NAICS Code:** 511210, 541511, 541512

    -   **GSA Schedule Number:** GS-35F-0119Y


    ### Export Control Classification


    -   **(ECCN):** 5D992.c

    -   **(CCATS):** G163509
  form:
    code: >-
      <form class=u-margin-top-xs id=mktoForm_1411></form>

      <script>
        //<![CDATA[
          MktoForms2.setOptions(
          {
            formXDPath : "/rs/194-VVC-221/images/marketo-xdframe-relative.html"
          });
          MktoForms2.loadForm("//page.gitlab.com", "194-VVC-221", 1411, function(form) {
            form.onSuccess(function(values, followUpUrl) {
          
              dataLayer.push(
              {
                'event' : 'public-sector', 
                'mktoFormId' : form.getId(),
                'eventCallback' : function()
                {
                  form.getFormElem().hide();
                  document.getElementById('confirmform').style.visibility = 'visible';
                }, 'eventTimeout' : 3000
              });
              return false;
            });
            function getgacid() {
              try {
                var tracker = ga.getAll()[0];
                return tracker.get('clientId');
              } catch (e) {
                return 'n/a';
              }
            }
            form.vals({
              'gacid': getgacid()
            });
          });
        //]]>
      </script>
